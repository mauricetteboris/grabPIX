# Analyse des données de certifications PIX : grabPIX

grabPix est un **script Python** qui permet, à partir des fichiers CSV des certifications de Pix Orga, de générer : 
- un **fichier tableur** regroupant plein de statistiques : moyenne, écart-type, min, max...

- **2 boites à moustaches** : nombre de pix et nombre de compétences, comme par exemple :

![](PIX-Nombre_de_competences_petit.png)


## Nouvelle version

Le nouveau script se nomme à présent : `grabPIX_altair.py`)

## Dépendances 

Ce script nécessite quelques modules pour le traitement des CSV :

**Linux**

    pip3 install numpy pandas altair vl-convert-python

**Windows**
    
	py -3 -m pip install numpy pandas altair vl-convert-python


## Documentation

La documentation se situe dans le script, en voici une copie :

1. On exporte les fichiers CSV depuis orga.pix.fr dans un même dossier,
2. On y place le script `grabPIX_altair.py`,
3. On ajuste le dictionnaire `groups` des regroupements de classes (ligne 55),
4. On l'exécute soit avec son IDE favori, soit avec la commande :
`python3 grabPIX_altair.py`
5. Les fichiers seront générés dans le dossier.